#!/usr/bin/env bash

OPENWRT_PATH=$1
START=$(dirname $(readlink -nf $0))/../
REV=`${START}/bin/getrev.sh ${OPENWRT_PATH}`

cd ${OPENWRT_PATH}

cp ${START}/config/openwrt_config ${OPENWRT_PATH}/.config

cp ${START}/config/openwrt_kernel_config ${OPENWRT_PATH}/target/linux/x86/config-3.3

cp ${START}/config/feeds.conf ${OPENWRT_PATH}/

cp ${START}/patches/003-kernel-imqmq.patch ${OPENWRT_PATH}/target/linux/x86/patches-3.3/
cp ${START}/patches/005-iptables-imq.patch ${OPENWRT_PATH}/package/iptables/patches/
patch -p0 < ${START}/patches/package_kernel_modules_netsupport.mk.diff
patch -p0 < ${START}/patches/package_kernel_modules_netfilter.mk.diff
patch -p0 < ${START}/patches/package_iptables_Makefile.diff
patch -p1 < ${START}/patches/busybox_pam.diff

patch -p0 < ${START}/patches/opkg_with_version.diff

cp ${START}/patches/passwd ${OPENWRT_PATH}/package/base-files/files/etc
cp ${START}/patches/shadow ${OPENWRT_PATH}/package/base-files/files/etc
patch -p1 < ${START}/patches/group.diff
mkdir ${OPENWRT_PATH}/package/base-files/files/home
cp ${START}/patches/securetty ${OPENWRT_PATH}/package/base-files/files/etc
cp ${START}/patches/999-pam-chpasswd.patch ${OPENWRT_PATH}/package/busybox/patches/999-pam-chpasswd.patch
cp ${START}/patches/600-ppp-rfc4638.patch ${OPENWRT_PATH}/package/ppp/patches/600-ppp-rfc4638.patch

cd ${OPENWRT_PATH}/package/base-files/files/etc
patch -p0 < ${START}/patches/inittab.diff

rm ${OPENWRT_PATH}/package/opkg/patches/009-remove-upgrade-all.patch

cd ${START}

