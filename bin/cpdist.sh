#!/usr/bin/env bash

OPENWRT_PATH=$1
DEST=$2
START=$(dirname $(readlink -nf $0))/../
REV=`${START}/bin/getrev.sh $OPENWRT_PATH`

rsync --delete-before -aPrc  --exclude=OpenWrt-SDK* --exclude=OpenWrt-Toolchain* --exclude=OpenWrt-ImageBuilder* ${OPENWRT_PATH}/bin/x86/* ${DEST}/dist/r${REV}/

