#!/usr/bin/env bash

START=$(dirname $(readlink -nf $0))/../
OPENWRT_PATH=$1

cd ${OPENWRT_PATH}
#REV=`svn info | grep Revision | cut -d ":" -f 2 | tr -d " "`
#REV=`git log -n1 --pretty=oneline | cut -f1 -d' '`
REV=`git log -n3 | grep commit | head -n1 | cut -d' ' -f2`
cd ${START}
echo ${REV}

