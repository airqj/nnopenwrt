#!/usr/bin/env bash

OPENWRT_PATH=$1
DEST=$2
START=$(dirname $(readlink -nf $0))/../
REV=`${START}/bin/getrev.sh $OPENWRT_PATH`

cd $DEST
mkdir dist/r${REV}
cd $START
