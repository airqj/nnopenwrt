#!/usr/bin/env bash

OPENWRT_PATH=$1
START=$(dirname $(readlink -nf $0))/../

cd $OPENWRT_PATH

# OpenWRT trunk SVN Sources
git checkout openwrt
git clean -dxf
git svn rebase
git push origin openwrt

# OpenWRT attitude adjustment SVN Sources
git checkout openwrt-attitude_adjustment
git clean -dxf
git svn rebase
git push origin openwrt-attitude_adjustment

# Packages feed SVN Sources
git checkout feed_packages
git clean -dxf
git svn rebase
git push origin feed_packages

# Packages feed attitude adjustment SVN Sources
git checkout feed_packages-12.09
git clean -dxf
git svn rebase
git push origin feed_packages-12.09

# xwrt feed SVN Sources
git checkout feed_xwrt
git clean -dxf
git svn rebase
git push origin feed_xwrt

# luci feed SVN Sources
git checkout feed_luci
git clean -dxf
git svn rebase
git push origin feed_luci

git checkout master
git clean -dxf
git push origin master

cd $START

